
		updateList();

		var plus = document.getElementById('plus');
		var close = document.getElementById('close');
		var refresh = document.getElementById('refresh');

		refresh.onclick = function(){
			alert("Lista atualizada");
			updateList();
		};

		plus.onclick = function(){
			var modal = document.getElementById('modal1');
			modal.style.display = "block";
		};

		close.onclick = function(){
			var modal = document.getElementById('modal1');
			modal.style.display = "none";
		}

		var nome = document.getElementById('nome').value;
		var texto = document.getElementById('texto').value;
		var data = new Date();
		var enviar = document.getElementById('enviar');

		document.forms["formulario"].addEventListener("submit",submit);

		function submit(){
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function(){
        if(this.readyState==4 && this.status==200){
          console.log("Funfou!");
          console.log(this.responseText);
          document.forms["formulario"]["nome"].value = "";
          document.forms["formulario"]["texto"].value = "";
        }
      }
      xhttp.open("POST","http://rest.learncode.academy/api/alleflobo/posts",true);
      xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      var body = "name=" + document.forms["formulario"]["nome"].value
                  + "&texto=" + document.forms["formulario"]["texto"].value
									+ "&data=" + data;
      xhttp.send(body);
			updateList();
			close.onclick();
    }

		function updateList(){
			var http = new XMLHttpRequest();
			http.onreadystatechange = function(){
				var posts = document.getElementById('posts');
				while (posts.firstChild) {
					posts.removeChild(posts.firstChild);
				}

					if(this.readyState == 4 && this.status == 200){
							var json = this.responseText;
							var obj = JSON.parse(json);
							console.log(obj);
							obj.reverse();
							for (variable of obj) {
								var div = document.createElement("div");
								var para = document.createElement("p");
								var br1 = document.createElement("br");
								var br2 = document.createElement("br");

								var nome = document.createTextNode("Nome: "+ variable['name']);
								var texto = document.createTextNode("Texto: "+ variable['texto']);
								var data = document.createTextNode("Created at: "+ variable['data']);

								para.appendChild(nome);
								para.appendChild(br1);

								para.appendChild(texto);
								para.appendChild(br2);

								para.appendChild(data);

								div.appendChild(para);
								div.className = "post";
								posts.appendChild(div);
							}
					}
			}
			http.open("GET","http://rest.learncode.academy/api/alleflobo/posts",true);
			http.send();
		}
