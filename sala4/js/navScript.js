window.onload = function() {
  var tutorialsAction = document.getElementById("tutorialsAction");
  var referencesAction = document.getElementById("referencesAction");
  var examplesAction = document.getElementById("examplesAction");

  var navTutorials = document.getElementById("navTutorials");
  var navReferences = document.getElementById("navReferences");
  var navExamples = document.getElementById("navExamples");

  tutorialsAction.onclick = function(){
    if (navTutorials.style.display == "none") {
        navTutorials.style.display = "block";
        navReferences.style.display = "none";
        navExamples.style.display = "none";
    }else{
      navTutorials.style.display = "none";
    }
  }

  referencesAction.onclick = function(){
    if (navReferences.style.display == "none") {
        navReferences.style.display = "block";
        navTutorials.style.display = "none";
        navExamples.style.display = "none";
    }else{
      navReferences.style.display = "none";
    }
  }

  examplesAction.onclick = function(){
    if (navExamples.style.display == "none") {
        navExamples.style.display = "block";
        navReferences.style.display = "none";
        navTutorials.style.display = "none";
    }else{
      navExamples.style.display = "none";
    }
  }

};
