$(document).ready(function(){

	if (localStorage.getItem('usuario')) {
		$('li.desativo').removeClass('desativo').addClass('ativo');
		$('footer.desativo').removeClass('desativo').addClass('ativo');

		$('#entrar').addClass('desativo');
		$('#cadastrar').addClass('desativo');
	}

	$('.cadastrar').on('click', function(){
		var cadastro = {
			nome: $('.usuario-cadastro').val().trim(),
			senha: $('.senha-cadastro').val().trim()
		};

		$.ajax({
      type:"GET",
      url:"http://rest.learncode.academy/api/alleflobo/usuarios",
      success: function (result) {
          for (variable of result) {
          	if (variable['nome'] == cadastro['nome']) {
							$('.container > .mensagens').empty();
          		$('.container > .mensagens').append("<div class='alert alert-danger'> <strong>Danger!</strong> Já existe um usuário com esse nome <span class='close'>&times;</span></div>");
							$(".close").click(function(){
	                $(this).parent().hide();
	            });
							return 1;
          	}
          }
					$.ajax({
						type:"POST",
						url:"http://rest.learncode.academy/api/alleflobo/usuarios",
						data: cadastro,
						success: function (data) {
								$('.container > .mensagens').empty();
								$('.container > .mensagens').append("<div class='alert alert-success'> <strong>Success!</strong> Cadastro efetuado. <span class='close'>&times;</span> </div>");
								$(".close").click(function(){
		                $(this).parent().hide();
		            });
								console.log(data);
						}
					});
      }
	  });
	});

	$('.entrar').on('click', function(){
		$.ajax({
      type:"GET",
      url: "http://rest.learncode.academy/api/alleflobo/usuarios",
      success: function(result){
				console.log(result);
				if (result.length !== 0) {
					for (variable of result) {
						if ($('.usuario-login').val().trim() == variable['nome'] && $('.senha-login').val().trim() == variable['senha']) {
							console.log(result);

							localStorage.setItem('usuario', variable['id']);

							localStorage.setItem('bone', JSON.stringify({
								'nome': 'Boné comum',
								'qtd': 0,
								'preco': 12.00,
								'precoTotal': 0.00
							}));

							localStorage.setItem('camisa', JSON.stringify({
								'nome': 'Camisa comum',
								'qtd': 0,
								'preco': 20.00,
								'precoTotal': 0.00
							}));

							localStorage.setItem('tenis', JSON.stringify({
								'nome': 'Tênis comum',
								'qtd': 0,
								'preco': 60.00,
								'precoTotal': 0.00
							}));


							$('li.desativo').removeClass('desativo').addClass('ativo');
							$('footer.desativo').removeClass('desativo').addClass('ativo');

							$('#entrar').addClass('desativo');
							$('#cadastrar').addClass('desativo');

							$('.container > .mensagens').empty();
							$('.container > .mensagens').append("<div class='alert alert-success'> <strong>Success!</strong> Login efetuado. <span class='close'>&times;</span> </div>");
							$(".close").click(function(){
	                $(this).parent().hide();
	            });
							return 1;
						}
					}
					$('.container > .mensagens').empty();
					$('.container > .mensagens').append("<div class='alert alert-danger'> <strong>Danger!</strong> Este usuário não está cadastrado <span class='close'>&times;</span></div>");
					$(".close").click(function(){
							$(this).parent().hide();
					});
				} else {
					$('.container > .mensagens').empty();
					$('.container > .mensagens').append("<div class='alert alert-danger'> <strong>Danger!</strong> Não há nenhum usuário cadastrado. <span class='close'>&times;</span></div>");
					$(".close").click(function(){
							$(this).parent().hide();
					});
				}

			}
    });
	});

	$('.sair').on('click', function(){
		localStorage.clear();
		$('li.ativo').removeClass('ativo').addClass('desativo');
		$('footer.ativo').removeClass('ativo').addClass('desativo');

		$('#entrar').removeClass('desativo');
		$('#cadastrar').removeClass('desativo');

		$('.container > .mensagens').empty();
		$('.container > .mensagens').append("<div class='alert alert-info'> <strong>Info!</strong> Você está deslogado. <span class='close'>&times;</span> </div>");
		$(".close").click(function(){
				$(this).parent().hide();
		});
	});

	$('.adicionar-bone').on('click', function(){
		var cesta = JSON.parse(localStorage.getItem('bone'));

		var qtd = $('.bone-qtd').val();
		cesta['qtd'] = parseInt(qtd) + parseInt(cesta['qtd']);
		cesta['precoTotal'] = (qtd * cesta['preco']) + parseInt(cesta['precoTotal']) ;

		localStorage.setItem('bone', JSON.stringify(cesta));

		console.log(localStorage.getItem('bone'));

		$('.container > .mensagens').empty();
		$('.container > .mensagens').append("<div class='alert alert-info'> <strong>Info!</strong> Adicionou " + qtd +" boné(s) <span class='close'>&times;</span> </div>");
		$(".close").click(function(){
				$(this).parent().hide();
		});
	});

	$('.adicionar-camisa').on('click', function(){
		var cesta = JSON.parse(localStorage.getItem('camisa'));

		var qtd = $('.camisa-qtd').val();
		cesta['qtd'] = parseInt(qtd) + parseInt(cesta['qtd']);
		cesta['precoTotal'] = (qtd * cesta['preco']) + parseInt(cesta['precoTotal']) ;

		localStorage.setItem('camisa', JSON.stringify(cesta));

		console.log(localStorage.getItem('camisa'));

		$('.container > .mensagens').empty();
		$('.container > .mensagens').append("<div class='alert alert-info'> <strong>Info!</strong> Adicionou " + qtd +" camisa(s) <span class='close'>&times;</span> </div>");
		$(".close").click(function(){
				$(this).parent().hide();
		});
	});

	$('.adicionar-tenis').on('click', function(){
		var cesta = JSON.parse(localStorage.getItem('tenis'));

		var qtd = $('.tenis-qtd').val();
		cesta['qtd'] = parseInt(qtd) + parseInt(cesta['qtd']);
		cesta['precoTotal'] = (qtd * cesta['preco']) + parseInt(cesta['precoTotal']) ;

		localStorage.setItem('tenis', JSON.stringify(cesta));

		console.log(localStorage.getItem('tenis'));

		$('.container > .mensagens').empty();
		$('.container > .mensagens').append("<div class='alert alert-info'> <strong>Info!</strong> Adicionou " + qtd +" tênis <span class='close'>&times;</span> </div>");
		$(".close").click(function(){
				$(this).parent().hide();
		});
	});

	$('.compras').on('click', function(){
		var bone = JSON.parse(localStorage.getItem('bone'));
		var camisa = JSON.parse(localStorage.getItem('camisa'));
		var tenis = JSON.parse(localStorage.getItem('tenis'));

		$('.cesta').empty();

		if (bone['qtd'] > 0) {
			$('.cesta').append("<div class='row'> <div class='col-sm-5'> <img src='img/bone.jpg'> </div> <div class='col-sm-4'> <h4>Boné comum</h2> <h6>Quantidade: "+bone['qtd']+" </h6> </div> <div class='col-sm-3'> <h4>R$ "+bone['preco']+"</h4> </div> </div>");
		}

		if (camisa['qtd'] > 0) {
			$('.cesta').append("<div class='row'> <div class='col-sm-5'> <img src='img/camisa.jpg'> </div> <div class='col-sm-4'> <h4>Camisa comum</h2> <h6>Quantidade: "+camisa['qtd']+" </h6> </div> <div class='col-sm-3'> <h4>R$ "+camisa['preco']+"</h4> </div> </div>");
		}

		if (tenis['qtd'] > 0) {
			$('.cesta').append("<div class='row'> <div class='col-sm-5'> <img src='img/tenis.jpg'> </div> <div class='col-sm-4'> <h4>Tênis comum</h2> <h6>Quantidade: "+tenis['qtd']+" </h6> </div> <div class='col-sm-3'> <h4>R$ "+tenis['preco']+"</h4> </div> </div>");
		}

		if (bone['qtd'] == 0 && camisa['qtd'] == 0 && tenis['qtd'] == 0) {
			$('.cesta').append("<h3>Não existe nenhum produto adicionado em sua cesta de compras.</h3>");
		}
	});

});
