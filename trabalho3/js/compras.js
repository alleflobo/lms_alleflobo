$(document).ready(function(){

	var bone = JSON.parse(localStorage.getItem('bone'));
	var camisa = JSON.parse(localStorage.getItem('camisa'));
	var tenis = JSON.parse(localStorage.getItem('tenis'));

	finalizando();
	feitas();

	function finalizando(){
		$('.tabela').empty();

		if (bone['qtd'] > 0) {
			$('.tabela').append("<tr>	<td>Boné comum</td>	<td> " +bone['qtd']+ " </td>	<td>R$ "+ bone['preco'] +"</td>	</tr>");
		}

		if (camisa['qtd'] > 0) {
			$('.tabela').append("<tr>	<td>Camisa comum</td>	<td> " + camisa['qtd'] +" </td>	<td>R$ "+ camisa['preco'] +"</td>	</tr>");
		}

		if (tenis['qtd'] > 0) {
			$('.tabela').append("<tr>	<td>Tênis comum</td>	<td> "+tenis['qtd']+" </td>	<td>R$ "+ tenis['preco']+"</td>	</tr>");
		}

		$('.total').append("Total: R$"+ (bone['precoTotal'] + camisa['precoTotal'] + tenis['precoTotal']) );
	};

	function feitas(){
		$.ajax({
      type:"GET",
      url: "http://rest.learncode.academy/api/alleflobo/compras",
      success: function(result){
				console.log(result);
				$('.feitas').empty();
				if (result.length !== 0) {
					for (variable of result) {
						if (variable['usuario'] === localStorage.getItem('usuario')) {
							$('.feitas').append("<tr> <td> "+ variable['data'] +" </td> <td>R$ "+ variable['valor'] +"</td> </tr>");
						}
					}
				}
			}
    });
	};

	$('.finalizar').on('click', function(){
		if ((bone['precoTotal'] + camisa['precoTotal'] + tenis['precoTotal']) > 0) {
			$.ajax({
	      type:"POST",
	      url:"http://rest.learncode.academy/api/alleflobo/compras",
				data: {
					usuario: localStorage.getItem('usuario'),
					valor: bone['precoTotal'] + camisa['precoTotal'] + tenis['precoTotal'],
					data: new Date()
				},
	      success: function (data) {
	          console.log("Funfou");
	          console.log(data);
						feitas();

						localStorage.setItem('bone', JSON.stringify({
							'nome': 'Boné comum',
							'qtd': 0,
							'preco': 12.00,
							'precoTotal': 0.00
						}));

						localStorage.setItem('camisa', JSON.stringify({
							'nome': 'Camisa comum',
							'qtd': 0,
							'preco': 20.00,
							'precoTotal': 0.00
						}));

						localStorage.setItem('tenis', JSON.stringify({
							'nome': 'Tênis comum',
							'qtd': 0,
							'preco': 60.00,
							'precoTotal': 0.00
						}));

						$('.container > .mensagens').empty();
						$('.container > .mensagens').append("<div class='alert alert-success'> <strong>Success!</strong> Compra efetuada. <span class='close'>&times;</span> </div>");
						$(".close").click(function(){
								$(this).parent().hide();
						});
						location.reload();
						return 1;
	      }
		  });
		} else {
			$('.container > .mensagens').empty();
			$('.container > .mensagens').append("<div class='alert alert-danger'> <strong>Danger!</strong> Não há nenhuma compra realizada. <span class='close'>&times;</span></div>");
			$(".close").click(function(){
					$(this).parent().hide();
			});
		}

	});

});
